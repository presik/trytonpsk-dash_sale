import logging
from io import BytesIO

import requests
from lxml import builder, etree
from requests.exceptions import Timeout

logger = logging.getLogger(__name__)

# url1 = "https://sipservicetest.azurewebsites.net/SIPService.asmx"  # url test
url1 = "https://sipserviceclientev6.azurewebsites.net/SIPService.asmx"
namespaces = {
    'xsi': "http://www.w3.org/2001/XMLSchema-instance",
    'xsd': "http://www.w3.org/2001/XMLSchema",
    'soapenv': "http://schemas.xmlsoap.org/soap/envelope/",
    'sip': "http://sip.net.co/",
}


def get_pay(data, terminal):

    soapenv = builder.ElementMaker(namespace=namespaces['soapenv'])
    element = builder.ElementMaker(namespace=namespaces['sip'])

    att_doc = builder.ElementMaker(nsmap=namespaces, namespace=namespaces['soapenv'])
    doc_xml = att_doc.Envelope(
        soapenv.Body(
            element.EscribirSolicitudPC(
                element.Terminal(terminal),
                element.Data(data),
                xmlns="http://sip.net.co",
            ),
        ),
    )
    xml_customer = etree.tostring(
        doc_xml,
        encoding='UTF-8',
        pretty_print=True,
        xml_declaration=True)

    headers = {
        # "Host": "sipservicetest.azurewebsites.net",
        "Host": "sipserviceclientev6.azurewebsites.net",
        "Content-Type": "text/xml; charset=utf-8",
        "Content-Length": str(len(xml_customer)),
        "SOAPAction": "http://sip.net.co/EscribirSolicitudPC",
    }
    try:
        response = requests.post(url1, data=xml_customer, headers=headers, timeout=10)
    except Timeout:
        response = None
    # file_ = open("/home/psk/redeban_test/escribirSolicitudV1.xml", "w")
    # file_.write(xml_customer.decode("utf-8"))
    # file_.close()
    # print(response, 'this is response')
    return response


def get_response_pay(terminal):

    soapenv = builder.ElementMaker(namespace=namespaces['soapenv'])
    element = builder.ElementMaker(namespace=namespaces['sip'])
    att_doc = builder.ElementMaker(nsmap=namespaces, namespace=namespaces['soapenv'])
    doc_xml = att_doc.Envelope(
        soapenv.Body(
            element.LeerRespuestaPC(
                element.Terminal(terminal),
                xmlns="http://sip.net.co",
            ),
        ),
    )
    xml_customer = etree.tostring(
        doc_xml,
        encoding='UTF-8',
        pretty_print=True,
        xml_declaration=True)

    headers = {
        # "Host": "sipservicetest.azurewebsites.net",
        "Host": "sipserviceclientev6.azurewebsites.net",
        "Content-Type": "text/xml; charset=utf-8",
        "Content-Length": str(len(xml_customer)),
        "SOAPAction": "http://sip.net.co/LeerRespuestaPC",
    }
    try:
        response = requests.post(url1, data=xml_customer, headers=headers, timeout=10)
        print(response.content)
    except Timeout:
        logger.exception(f'TimeOut error {url1}')
        response = None
    # file_ = open("/home/psk/redeban_test/leerRespuestaV1.xml", "w")
    # file_.write(xml_customer.decode("utf-8"))
    # file_.close()
    return response


def process_response(response):
    response_text = None
    if response:
        if response.status_code == 200:
            xml_response = BytesIO(response.content)
            for _, element in etree.iterparse(xml_response):
                action = etree.QName(element).localname
                if action in ('EscribirSolicitudPCResult', 'LeerRespuestaPCResult') and element.text:
                    response_text = element.text
        else:
            logger.warning(f'request payment redeban, {response.status_code}-{response.text}')
    return response_text


def get_dict_response_pay(data):
    values = data.split(',')
    keys = [
        'respuesta', 'codigo_aprobacion', 'bin_tarjeta',
        'tipo_cuenta', 'franquicia', 'monto_transaccion', 'iva_transaccion',
        'base_devolucion', 'impuesto_consumo', 'recibo_datafono', 'num_cuotas',
        'consecutivo_transaccion', 'numero_terminal', 'codigo_establecimiento',
        'fecha', 'hora', 'num_bono',
    ]
    return {keys[i]: values[i] for i in range(len(values))}


def test_response_pay(args):
    if args.get('intent') >= 3:
        r = ','.join(['0', 'NJ3234', 'NJDS', 'DB', '', '18000', '', '', '', 'HU3234', '', '', '', '', '', '', ''])
        # r = ','.join(['1','NJ3234','NJDS','CR', '', '', '','', '', 'HU3234','', '', '',  '', '', '', ''])
    else:
        r = None
    return r
