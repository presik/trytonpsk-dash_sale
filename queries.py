
query_sale = """
    SELECT i.sale_date AS date,
    i.state,
    i.id as record_sale,
    l.quantity, l.unit_price
    FROM sale_sale AS i
    INNER JOIN sale_line AS l ON l.sale=i.id
    WHERE
    i.sale_date >= '{start_date}'
    AND i.sale_date <= '{end_date}'
    AND i.state in ('draft', 'quotation')
    {where}
"""


query_invoice = """
    WITH
    admin_category AS (
        SELECT c.name AS category, cat.template
        FROM (
            SELECT DISTINCT template
            template, category
            FROM "product_template-product_category"
        ) AS cat
        LEFT JOIN product_category as c ON c.id=cat.category
    )
    SELECT sh.name as shop_name, i.invoice_date AS date,
        i.number, i.state,
        apt.name AS payment_term, i.id as record_sale,
        apt.payment_type, i.party,
        l.product, pt.code AS product_code, pt.name AS product_name,
        adc.category AS category_admin, l.quantity, l.unit, l.unit_price,
        uom_s.symbol as uom
    FROM account_invoice AS i
    INNER JOIN account_invoice_line AS l ON l.invoice=i.id
    INNER JOIN product_product AS p on l.product=p.id
    INNER JOIN product_template AS pt on p.template=pt.id
    INNER JOIN product_uom AS uom_s ON l.unit=uom_s.id
    INNER JOIN account_invoice_payment_term AS apt on i.payment_term=apt.id
    LEFT JOIN product_category AS pcc on pt.account_category=pcc.id
    LEFT JOIN admin_category AS adc on pt.id=adc.template
    LEFT JOIN sale_shop AS sh ON i.shop=sh.id
    WHERE
        i.invoice_date >= '{start_date}'
        AND i.invoice_date <= '{end_date}'
    {where}
"""


sale_by_period = """
    SELECT
        SUM(CASE WHEN inv.invoice_date>='{start_current}' AND inv.invoice_date<='{end_current}' THEN li.quantity * li.unit_price ELSE 0 END) AS amount,
        SUM(CASE WHEN inv.invoice_date>='{start_prev}' AND inv.invoice_date<='{end_prev}' THEN li.quantity * li.unit_price ELSE 0 END) AS prev_amount
    FROM account_invoice AS inv
    INNER JOIN account_invoice_line AS li ON li.invoice=inv.id
    WHERE
        inv.invoice_date>='{start_prev}' AND inv.invoice_date<='{end_current}' AND
        inv.state IN ('posted', 'paid', 'validated') AND inv.type='out'
"""
