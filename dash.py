# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta


class DashApp(metaclass=PoolMeta):
    __name__ = 'dash.app'

    @classmethod
    def _get_origin(cls):
        origins = super(DashApp, cls)._get_origin()
        origins.extend([
            'dash.app.delivery',
            'dash.app.sale_order',
            'dash.app.take_order',
            'dash.app.order_viewer',
            'dash.app.self_service_sale',
            'dash.app.sale_call_center',
            'dash.app.sale_turn',
            'dash.app.order_notification',
            'dash.app.sale_audit',
        ])
        return origins

    @classmethod
    def get_selection(cls):
        options = super(DashApp, cls).get_selection()
        options.extend([
            ('delivery', 'Delivery'),
            ('sale_order', 'Sale Order'),
            ('take_order', 'Take Order'),
            ('order_viewer', 'Order Viewer'),
            ('self_service_sale', 'Self-Service Sale'),
            ('sale_call_center', 'Sale Call Center'),
            ('sale_turn', 'Sale Turn'),
            ('order_notification', 'Order Notification'),
            ('sale_audit', 'Sale Audit'),
        ])
        return options
