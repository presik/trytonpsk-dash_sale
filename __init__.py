# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import dash


def register():
    Pool.register(
        sale.Sale,
        sale.AppDelivery,
        sale.AppSaleOrder,
        sale.AppTakeOrder,
        sale.AppOrderViewer,
        sale.AppSaleCallCenter,
        sale.AppSelfServiceSale,
        sale.AppSaleTurn,
        sale.AppOrderNotification,
        sale.AppSaleAudit,
        dash.DashApp,
        module='dash_sale', type_='model')
