import polars as pol

from trytond.transaction import Transaction


def polar_query(query, args):
    cursor = Transaction().connection.cursor()
    _query = query.format(**args)
    return pol.read_database(_query, cursor)
