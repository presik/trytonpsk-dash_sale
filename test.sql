

SELECT
    SUM(CASE WHEN inv.invoice_date>='2024-01-01' AND inv.invoice_date<='2024-05-31' THEN li.quantity * li.unit_price ELSE 0 END) AS amount,
    SUM(CASE WHEN inv.invoice_date>='2023-01-01' AND inv.invoice_date<='2023-05-31' THEN li.quantity * li.unit_price ELSE 0 END) AS prev_amount
FROM account_invoice AS inv
INNER JOIN account_invoice_line AS li ON li.invoice=inv.id
WHERE
    inv.invoice_date>='2023-01-01' AND inv.invoice_date<='2024-05-31' AND
    inv.state IN ('posted', 'paid', 'validated') AND inv.type='out';
